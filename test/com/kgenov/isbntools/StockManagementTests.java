package com.kgenov.isbntools;
/* konstantin created on 2/2/2021 inside the package - com.kgenov.isbntools */

import org.junit.Test;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.fail;
import static org.mockito.Mockito.*;

public class StockManagementTests {

    @Test
    public void testCanGetACorrectLocatorCode(){
        //simulate external data service
        ExternalISBNDataService testWebService = new ExternalISBNDataService() {
            @Override
            public Book lookup(String isbn) {
                return new Book(isbn, "Of Mice And Men", "J. Steinbeck");
            }
        };

        ExternalISBNDataService testDBService = new ExternalISBNDataService() {
            @Override
            public Book lookup(String isbn) {
                return null;
            }
        };

        StockManager stockManager = new StockManager();
        stockManager.setWebService(testWebService); // inject service
        stockManager.setDatabaseService(testDBService);

        String isbn = "0140177396";
        String locatorCode = stockManager.getLocatorCode(isbn);
        assertEquals("7396J4", locatorCode);
    }

    // this test checks if we are calling our DB lookup method when we have the data present, instead of directly calling the 3rd party API
    @Test
    public void databaseIsUsedIfDataPresent(){
        ExternalISBNDataService databaseService = mock(ExternalISBNDataService.class);
        ExternalISBNDataService webService = mock(ExternalISBNDataService.class);

        // mockito allows us to provide an implementation of our methods, we can specify the input and the output
        when(databaseService.lookup("0140177396")).thenReturn(new Book("0140177396", "abc", "abc"));

        StockManager stockManager = new StockManager();
        stockManager.setWebService(webService);
        stockManager.setDatabaseService(databaseService);

        String isbn = "0140177396";
        String locatorCode = stockManager.getLocatorCode(isbn);

        // verify the number of times a method from a specific class was called - lookup 1 time
        // here we need to verify we are calling the dbService implementation over the WS impl.
        verify(databaseService, times(1)).lookup("0140177396"); // correct

        // verify we are NOT calling the WS impl. ;; Note: anyString specifies a random string value, here we don't care about a specific ISBN
        verify(webService, times(0)).lookup(anyString()); // correct
    }

    @Test
    public void webServiceIsUsedIfDataNotPresentInDatabase(){
        fail();
    }

}
