package com.kgenov.isbntools;

import org.junit.Test;

import static org.junit.Assert.*;

public class ValidateISBNTest {

    @Test
    public void checkAValid10DigitISBN(){
        ValidateISBN validator = new ValidateISBN();
        boolean validationResult = validator.checkISBN("0140449116");
        assertTrue("first value", validationResult);
        validationResult = validator.checkISBN("0140177396");
        assertTrue("second value", validationResult);
    }

    @Test
    public void checkAValid13DigitISBN(){
        ValidateISBN validator = new ValidateISBN();
        boolean validationResult = validator.checkISBN("9781853260087");
        assertTrue(validationResult);
    }

    @Test
    public void checkAInvalid10DigitISBN(){
        ValidateISBN validator = new ValidateISBN();
        boolean validationResult = validator.checkISBN("0140449117");
        assertFalse(validationResult);
    }

    @Test
    public void checkAInvalid13DigitISBN(){
        ValidateISBN validator = new ValidateISBN();
        boolean validationResult = validator.checkISBN("9781853267336");
        assertFalse(validationResult);
    }

    @Test(expected = NumberFormatException.class) // here we can specify the exception we are expecting from the method call
    public void nineDigitISBNsAreNotAllowed() {
        ValidateISBN validator = new ValidateISBN();
        validator.checkISBN("123456789");

    }

    @Test(expected = NumberFormatException.class)
    public void onlyDigitsInISBNsAreAllowed(){
        ValidateISBN validator = new ValidateISBN();
        validator.checkISBN("helloworld");
    }

    @Test
    public void TenDigitsISBNNumbersEndingInXAreValid(){
        ValidateISBN validator = new ValidateISBN();
        boolean result = validator.checkISBN("012000030X");
        assertTrue(result);
    }

}