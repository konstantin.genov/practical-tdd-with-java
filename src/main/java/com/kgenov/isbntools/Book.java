package com.kgenov.isbntools;
/* konstantin created on 2/2/2021 inside the package - com.kgenov.isbntools */

public class Book {
    private String isbn, title, author;

    public Book(String isbn, String title, String author) {
        this.isbn = isbn;
        this.title = title;
        this.author = author;
    }

    public String getIsbn() {
        return isbn;
    }

    public String getTitle() {
        return title;
    }

    public String getAuthor() {
        return author;
    }
}
