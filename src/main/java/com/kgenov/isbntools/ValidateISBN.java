package com.kgenov.isbntools;
/* konstantin created on 1/31/2021 inside the package - com.kgenov.isbntools */

public class ValidateISBN {

    private static final int SHORT_ISBN_LENGTH = 10;
    private static final int LONG_ISBN_LENGTH = 13;

    public boolean checkISBN(String isbn) {
        if (isbn.length() == LONG_ISBN_LENGTH) {
            return longISBNValidation(isbn);
        } else if (isbn.length() == SHORT_ISBN_LENGTH) {
            return shortISBNValidation(isbn);
        }
        throw new NumberFormatException("ISBN numbers must be 10 digits or 13 digits long!");

    }

    private boolean longISBNValidation(String isbn) {
        int total = 0;

        for (int i = 0; i < LONG_ISBN_LENGTH; i++) {
            if (i % 2 == 0) {
                total += Character.getNumericValue(isbn.charAt(i));
            } else {
                total += Character.getNumericValue(isbn.charAt(i)) * 3;
            }
        }
        return total % 10 == 0;
    }

    private boolean shortISBNValidation(String isbn) {
        int total = 0;

        for (int i = 0; i < SHORT_ISBN_LENGTH; i++) {
            if (!Character.isDigit(isbn.charAt(i))) {
                if (i == 9 && isbn.charAt(i) == 'X') {
                    total += 10;
                } else {
                    throw new NumberFormatException("ISBN numbers can only contain digits!");
                }
            } else {
                total += Character.getNumericValue(isbn.charAt(i)) * (10 - i);
            }
        }

        return total % 11 == 0;
    }
}
