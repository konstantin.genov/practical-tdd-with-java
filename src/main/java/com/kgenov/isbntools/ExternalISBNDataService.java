package com.kgenov.isbntools;

public interface ExternalISBNDataService {
    public Book lookup(String isbn);
}
